# Details {#details}

A comprehensive documentation of GitLab Pages can be found here: <https://docs.gitlab.com/ee/user/project/pages>.  
In order to use Gitlab Pages, you must configure GitLab CI: the configuration is declared in a YAML file named `.gitlab-ci.yml`.

## Docker image

Here, the configuration file begins with an `image` declaration. It indicates to the GitLab Runner which [Docker](https://www.docker.com/) image must be used by the CI job.

```{r find-image-name, comment=NA, class.output='yaml', echo=FALSE}
image <- 
  readLines("./.gitlab-ci.yml") %>%
  stringr::str_subset("rocker") %T>%
  cat() %>%
  yaml::yaml.load() %$%
  image %>%
  stringr::str_split(":") %>%
  unlist()
```

In order to build a website with `bookdown`, you have to choose a [Docker](https://www.docker.com/) image with **bookdown** installed. In this example, the `r image[1] %>% paste0("[\u0060", ., "\u0060](https://hub.docker.com/r/", ., ")")` image is used: the [Rocker project](https://www.rocker-project.org/) provides the [official R image for Docker](http://dirk.eddelbuettel.com/blog/2014/12/19/). If you are new with [Docker](https://www.docker.com/), I highly recommend this [rOpenSci](https://ropensci.org/)'s [Docker tutorial for reproducible research](http://ropenscilabs.github.io/r-docker-tutorial/). 

Using a Rocker image, **you can ensure the reproducibility of your bookdown website**.

## Jobs definition

The jobs are defined by the remaining lines of the `.gitlab-ci.yml` file:

```{r extract-pages-job, comment=NA, class.output='yaml', echo=FALSE}
readChar("./.gitlab-ci.yml", 1e5) %>%
  strsplit("(?=\\n\\.bookdown:)", perl = TRUE) %>%
  unlist() %>%
  stringr::str_subset("\\.bookdown") %>%
  cat()
```

Here, we define 3 jobs:

- a hidden job named `.bookdown` (the leading dot is used to declare a hidden job). This hidden job acts as a template for the two other jobs. This job declares the script used to build your **bookdown** book and indicates that the output files are located in the `public` folder (these are the artefacts).

- a `pages` job. This job is required to serve your **bookdown** book with GitLab Pages. It is highly recommended to run this job only when the `master` branch is modified.

- a `mr-review` job. This job will allow you to **preview the result of a pull request** (with GitLab, pull requests are named merge requests). This job declares a different environment for each pull request. You can access to the preview site in the merge request page^[The environment declaration uses two tricks: the first one is to dynamically build the url and was inspired by [this SO answer](https://stackoverflow.com/a/58402821). The second one is to pass this url using a [`dotenv` report](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)].

## Add a review application to your pull request previews

There is a hidden gem in GitLab CI: [**the review application**](https://docs.gitlab.com/ee/ci/review_apps/). This is a basic application that can be used by non git users to send feedbacks on your **bookdown** preview (it is only available for merge requests previews). Their comments directly appear in the merge request page.

In order to use this application, you must insert an HTML `<script>` tag in the `<head>` tag of your **bookdown** website. 

You need to modify two things in your **bookdown** source files : 

- the first one is to dynamically build this `<script>` tag. You can insert the following code in an R code chunk (with `echo=FALSE`), it will create a file named `review-app.html` which contains the code to load the review application:

```{r gitlab-review-app, eval=FALSE, echo=TRUE}
```

- the second one is to include the `review-app.html` file in the **bookdown** header. You have to modify the output format options as follows:

```
output:
  bookdown::gitbook: 
    includes:
      in_header: review-app.html
```
