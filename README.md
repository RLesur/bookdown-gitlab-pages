# Get the full power of GitLab CI for your bookdown project

This repository is a bookdown demo website served on `GitLab Pages` using `GitLab CI`.

See https://rlesur.gitlab.io/bookdown-gitlab-pages
